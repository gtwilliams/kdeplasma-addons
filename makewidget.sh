#!/usr/bin/sh

applet_path="$HOME/.local/share/plasma/plasmoids/org.kde.plasma.systemloadviewer"
if [ ! -d "${applet_path}" ]
then
    echo "Creating directory "${applet_path}" ..."
    mkdir -p "${applet_path}"
    echo "Copying applet files to new directory ..."
    cp -ur applets/systemloadviewer/package/* "${applet_path}"
    echo "Done."
else
    white true
    do
        echo "Warning: Directory "${applet_path}" exists already."
        read -rp "Do you want to overwrite files there? [y/n]: " yn
        case $yn in
            y)
                cp -ur applets/systemloadviewer/package/* "${applet_path}"
                echo "Done."
                break;;
            n)
                break;;
            *)
                echo "Please type y for yes or n for no"
        esac
    done
fi
