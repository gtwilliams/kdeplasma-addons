# kdeplasma-addons:
- Purpose of this repo is to preserve the System Load Viewer widget that got removed from [KDE Plasma](https://invent.kde.org/plasma/kdeplasma-addons) as of version 5.21.
- To serve as a space for discussions and improvements from the community that could land in future versions of the widget.
- You can find and/or install the widget from [the KDE store website](https://store.kde.org/p/1474921)

## Content:
- The main branch contains just the systemloadviewer applet. It is a CPU/RAM/SWAP monitor applet that you can put on the panel/taskmanager
to always have a look at the system resource usage.
- Other addons/applets can be found in the original Plasma branch.

## Insall:
- From within Plasma itself (recommended).
    - Right click the taskmanager/panel or desktop and click "Add Widgets"
    - Then click "Get New Widgets", search for "System Load Viewer"
    - Again right click the taskmanager/panel or desktop, Add Widgets and double click this time the new installed one from the list.

- From [the KDE store website](https://store.kde.org/p/1474921)
- If you prefer the terminal, run the following commands:

````sh
git clone --branch main --single-branch https://gitlab.com/hkanjal/kdeplasma-addons.git
cd kdeplasma-addons
chmod u+x makewidget.sh
./makewidget.sh
````
